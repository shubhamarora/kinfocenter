# kcmdevinfo Bahasa Melayu (Malay) (ms)
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sharuzzaman Ahmat Raslan <sharuzzaman@myrealbox.com>, 2010.
# Sharuzzaman Ahmat Raslan <sharuzzaman@gmail.com>, 2010, 2011.
msgid ""
msgstr ""
"Project-Id-Version: kcmdevinfo\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-19 02:06+0000\n"
"PO-Revision-Date: 2011-05-11 16:42+0800\n"
"Last-Translator: Sharuzzaman Ahmat Raslan <sharuzzaman@gmail.com>\n"
"Language-Team: Malay <kedidiemas@yahoogroups.com>\n"
"Language: ms\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=1;\n"

#: devicelisting.cpp:41
#, kde-format
msgctxt "Device Listing Whats This"
msgid "Shows all the devices that are currently listed."
msgstr ""

#: devicelisting.cpp:44
#, kde-format
msgid "Devices"
msgstr "Peranti"

#: devicelisting.cpp:57
#, kde-format
msgid "Collapse All"
msgstr "Runtuhkan Semua"

#: devicelisting.cpp:60
#, kde-format
msgid "Expand All"
msgstr "Kembangkan Semua"

#: devicelisting.cpp:63
#, kde-format
msgid "Show All Devices"
msgstr ""

#: devicelisting.cpp:66
#, kde-format
msgid "Show Relevant Devices"
msgstr ""

#: devicelisting.cpp:95
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "unknown device type"
msgid "Unknown"
msgstr "Tidak diketahui"

#: devicelisting.cpp:136 devinfo.cpp:73
#, fuzzy, kde-format
#| msgid "None"
msgctxt "no device UDI"
msgid "None"
msgstr "Tiada"

#: devinfo.cpp:52
#, kde-format
msgid "UDI: "
msgstr ""

#: devinfo.cpp:60
#, kde-format
msgctxt "Udi Whats This"
msgid "Shows the current device's UDI (Unique Device Identifier)"
msgstr ""

#: infopanel.cpp:20
#, kde-format
msgid "Device Information"
msgstr "Maklumat Peranti"

#: infopanel.cpp:29
#, kde-format
msgctxt "Info Panel Whats This"
msgid "Shows information about the currently selected device."
msgstr ""

#: infopanel.cpp:56
#, kde-format
msgid ""
"\n"
"Solid Based Device Viewer Module"
msgstr ""

#: infopanel.cpp:119
#, kde-format
msgid "Description: "
msgstr ""

#: infopanel.cpp:121
#, kde-format
msgid "Product: "
msgstr ""

#: infopanel.cpp:123
#, kde-format
msgid "Vendor: "
msgstr ""

#: infopanel.cpp:145
#, kde-format
msgid "Yes"
msgstr "Ya"

#: infopanel.cpp:147
#, kde-format
msgid "No"
msgstr "Tidak"

#: infopanel.h:36
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "name of something is not known"
msgid "Unknown"
msgstr "Tidak diketahui"

#: soldevice.cpp:65
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "unknown device"
msgid "Unknown"
msgstr "Tidak diketahui"

#: soldevice.cpp:91
#, kde-format
msgctxt "Default device tooltip"
msgid "A Device"
msgstr ""

#: soldevicetypes.cpp:43
#, kde-format
msgid "Processors"
msgstr ""

#: soldevicetypes.cpp:59
#, kde-format
msgid "Processor %1"
msgstr ""

#: soldevicetypes.cpp:76
#, kde-format
msgid "Intel MMX"
msgstr ""

#: soldevicetypes.cpp:79
#, kde-format
msgid "Intel SSE"
msgstr ""

#: soldevicetypes.cpp:82
#, kde-format
msgid "Intel SSE2"
msgstr ""

#: soldevicetypes.cpp:85
#, kde-format
msgid "Intel SSE3"
msgstr ""

#: soldevicetypes.cpp:88
#, kde-format
msgid "Intel SSSE3"
msgstr ""

#: soldevicetypes.cpp:91
#, kde-format
msgid "Intel SSE4.1"
msgstr ""

#: soldevicetypes.cpp:94
#, kde-format
msgid "Intel SSE4.2"
msgstr ""

#: soldevicetypes.cpp:97
#, kde-format
msgid "AMD 3DNow!"
msgstr ""

#: soldevicetypes.cpp:100
#, kde-format
msgid "ATI IVEC"
msgstr ""

#: soldevicetypes.cpp:103
#, fuzzy, kde-format
#| msgid "None"
msgctxt "no instruction set extensions"
msgid "None"
msgstr "Tiada"

#: soldevicetypes.cpp:106
#, kde-format
msgid "Processor Number: "
msgstr ""

#: soldevicetypes.cpp:106
#, kde-format
msgid "Max Speed: "
msgstr ""

#: soldevicetypes.cpp:107
#, kde-format
msgid "Supported Instruction Sets: "
msgstr ""

#: soldevicetypes.cpp:132
#, kde-format
msgid "Storage Drives"
msgstr ""

#: soldevicetypes.cpp:151
#, kde-format
msgid "Hard Disk Drive"
msgstr ""

#: soldevicetypes.cpp:154
#, kde-format
msgid "Compact Flash Reader"
msgstr ""

#: soldevicetypes.cpp:157
#, kde-format
msgid "Smart Media Reader"
msgstr ""

#: soldevicetypes.cpp:160
#, kde-format
msgid "SD/MMC Reader"
msgstr ""

#: soldevicetypes.cpp:163
#, fuzzy, kde-format
msgid "Optical Drive"
msgstr "Jenis peranti pemacu optikal"

#: soldevicetypes.cpp:166
#, kde-format
msgid "Memory Stick Reader"
msgstr ""

#: soldevicetypes.cpp:169
#, kde-format
msgid "xD Reader"
msgstr ""

#: soldevicetypes.cpp:172
#, kde-format
msgid "Unknown Drive"
msgstr ""

#: soldevicetypes.cpp:192
#, kde-format
msgid "IDE"
msgstr "IDE"

#: soldevicetypes.cpp:195
#, kde-format
msgid "USB"
msgstr "USB"

#: soldevicetypes.cpp:198
#, kde-format
msgid "IEEE1394"
msgstr ""

#: soldevicetypes.cpp:201
#, kde-format
msgid "SCSI"
msgstr "SCSI"

#: soldevicetypes.cpp:204
#, kde-format
msgid "SATA"
msgstr ""

#: soldevicetypes.cpp:207
#, fuzzy, kde-format
#| msgid "Platform"
msgctxt "platform storage bus"
msgid "Platform"
msgstr "Platform"

#: soldevicetypes.cpp:210
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "unknown storage bus"
msgid "Unknown"
msgstr "Tidak diketahui"

#: soldevicetypes.cpp:213
#, kde-format
msgid "Bus: "
msgstr ""

#: soldevicetypes.cpp:213
#, kde-format
msgid "Hotpluggable?"
msgstr ""

#: soldevicetypes.cpp:213
#, kde-format
msgid "Removable?"
msgstr ""

#: soldevicetypes.cpp:257
#, kde-format
msgid "Unused"
msgstr "Tidak diguna"

#: soldevicetypes.cpp:260
#, fuzzy, kde-format
msgid "File System"
msgstr "Sistem Fail"

#: soldevicetypes.cpp:263
#, kde-format
msgid "Partition Table"
msgstr ""

#: soldevicetypes.cpp:266
#, fuzzy, kde-format
msgid "Raid"
msgstr "Raid"

#: soldevicetypes.cpp:269
#, fuzzy, kde-format
msgid "Encrypted"
msgstr "Disulitkan"

#: soldevicetypes.cpp:272
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "unknown volume usage"
msgid "Unknown"
msgstr "Tidak diketahui"

#: soldevicetypes.cpp:275
#, kde-format
msgid "File System Type: "
msgstr ""

#: soldevicetypes.cpp:275
#, kde-format
msgid "Label: "
msgstr ""

#: soldevicetypes.cpp:276
#, kde-format
msgid "Not Set"
msgstr ""

#: soldevicetypes.cpp:276
#, kde-format
msgid "Volume Usage: "
msgstr ""

#: soldevicetypes.cpp:276
#, kde-format
msgid "UUID: "
msgstr ""

#: soldevicetypes.cpp:282
#, kde-format
msgid "Mounted At: "
msgstr ""

#: soldevicetypes.cpp:282
#, kde-format
msgid "Not Mounted"
msgstr ""

#: soldevicetypes.cpp:287
#, kde-format
msgid "Volume Space:"
msgstr ""

#: soldevicetypes.cpp:297
#, kde-format
msgctxt "Available space out of total partition size (percent used)"
msgid "%1 free of %2 (%3% used)"
msgstr ""

#: soldevicetypes.cpp:303
#, kde-format
msgid "No data available"
msgstr ""

#: soldevicetypes.cpp:330
#, kde-format
msgid "Multimedia Players"
msgstr ""

#: soldevicetypes.cpp:349 soldevicetypes.cpp:388
#, kde-format
msgid "Supported Drivers: "
msgstr ""

#: soldevicetypes.cpp:349 soldevicetypes.cpp:388
#, kde-format
msgid "Supported Protocols: "
msgstr ""

#: soldevicetypes.cpp:369
#, kde-format
msgid "Cameras"
msgstr ""

#: soldevicetypes.cpp:408
#, kde-format
msgid "Batteries"
msgstr ""

#: soldevicetypes.cpp:430
#, kde-format
msgid "PDA"
msgstr ""

#: soldevicetypes.cpp:433
#, kde-format
msgid "UPS"
msgstr ""

#: soldevicetypes.cpp:436
#, fuzzy, kde-format
msgid "Primary"
msgstr "Utama"

#: soldevicetypes.cpp:439
#, fuzzy, kde-format
msgid "Mouse"
msgstr "Tetikus"

#: soldevicetypes.cpp:442
#, fuzzy, kde-format
msgid "Keyboard"
msgstr "Papan Kekunci"

#: soldevicetypes.cpp:445
#, kde-format
msgid "Keyboard + Mouse"
msgstr ""

#: soldevicetypes.cpp:448
#, fuzzy, kde-format
msgid "Camera"
msgstr "Kamera"

#: soldevicetypes.cpp:451
#, kde-format
msgid "Phone"
msgstr ""

#: soldevicetypes.cpp:454
#, kde-format
msgctxt "Screen"
msgid "Monitor"
msgstr ""

#: soldevicetypes.cpp:457
#, kde-format
msgctxt "Wireless game pad or joystick battery"
msgid "Gaming Input"
msgstr ""

#: soldevicetypes.cpp:460
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "unknown battery type"
msgid "Unknown"
msgstr "Tidak diketahui"

#: soldevicetypes.cpp:466
#, fuzzy, kde-format
msgid "Charging"
msgstr "Mengecas"

#: soldevicetypes.cpp:469
#, kde-format
msgid "Discharging"
msgstr ""

#: soldevicetypes.cpp:472
#, kde-format
msgid "Fully Charged"
msgstr ""

#: soldevicetypes.cpp:475
#, kde-format
msgid "No Charge"
msgstr ""

#: soldevicetypes.cpp:478
#, kde-format
msgid "Battery Type: "
msgstr ""

#: soldevicetypes.cpp:478
#, kde-format
msgid "Charge Status: "
msgstr ""

#: soldevicetypes.cpp:478
#, kde-format
msgid "Charge Percent: "
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Sharuzzaman Ahmat Raslan"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sharuzzaman@gmail.com"

#, fuzzy
#~| msgid "Devices"
#~ msgid "Device Viewer"
#~ msgstr "Peranti"

#~ msgid "(c) 2010 David Hubner"
#~ msgstr "(c) 2010 David Hubner"

#, fuzzy
#~ msgid "Network Interfaces"
#~ msgstr "Antara Muka Rangkaian"

#~ msgid "Connected"
#~ msgstr "Tersambung"

#~ msgid "Wireless"
#~ msgstr "Tanpa wayar"

#, fuzzy
#~ msgid "Control"
#~ msgstr "Kawalan"

#, fuzzy
#~ msgid "Input"
#~ msgstr "Masukan"

#, fuzzy
#~ msgid "Output"
#~ msgstr "Output"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown audio interface type"
#~ msgid "Unknown"
#~ msgstr "Tidak diketahui"

#, fuzzy
#~ msgid "Modem"
#~ msgstr "Modem"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown sound card type"
#~ msgid "Unknown"
#~ msgstr "Tidak diketahui"

#, fuzzy
#~ msgid "Audio"
#~ msgstr "Audio"

#, fuzzy
#~ msgid "Network"
#~ msgstr "Rangkaian"

#, fuzzy
#~ msgid "Video"
#~ msgstr "Video"

#, fuzzy
#~| msgid "Platform"
#~ msgctxt "platform serial interface type"
#~ msgid "Platform"
#~ msgstr "Platform"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown serial interface type"
#~ msgid "Unknown"
#~ msgstr "Tidak diketahui"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown port"
#~ msgid "Unknown"
#~ msgstr "Tidak diketahui"

#, fuzzy
#~ msgid "Port: "
#~ msgstr "Port: "

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown smart card type"
#~ msgid "Unknown"
#~ msgstr "Tidak diketahui"

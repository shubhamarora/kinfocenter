# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Phuwanat Sakornsakolpat <narachai@gmail.com>, 2010.
# Thanomsub Noppaburana <donga.nb@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-05 02:14+0000\n"
"PO-Revision-Date: 2010-12-17 20:58+0700\n"
"Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>\n"
"Language-Team: Thai <thai-l10n@googlegroups.com>\n"
"Language: th\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.1\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: networkmodel.cpp:161
#, fuzzy, kde-format
#| msgid "Point to Point"
msgctxt "@item:intable Network type"
msgid "Point to Point"
msgstr "จุดต่อจุด"

#: networkmodel.cpp:168
#, fuzzy, kde-format
#| msgctxt "@item:intext Mode of network card"
#| msgid "Broadcast"
msgctxt "@item:intable Network type"
msgid "Broadcast"
msgstr "กระจายสัญญาณ"

#: networkmodel.cpp:175
#, fuzzy, kde-format
#| msgid "Multicast"
msgctxt "@item:intable Network type"
msgid "Multicast"
msgstr "มัลติแคสต์"

#: networkmodel.cpp:182
#, fuzzy, kde-format
#| msgid "Loopback"
msgctxt "@item:intable Network type"
msgid "Loopback"
msgstr "วนกลับ"

#: ui/main.qml:29
#, fuzzy, kde-format
#| msgid "Name"
msgctxt "@label"
msgid "Name:"
msgstr "ชื่อ"

#: ui/main.qml:33
#, fuzzy, kde-format
#| msgid "IP Address"
msgctxt "@label"
msgid "Address:"
msgstr "ที่อยู่ IP"

#: ui/main.qml:37
#, fuzzy, kde-format
#| msgid "Network Mask"
msgctxt "@label"
msgid "Network Mask:"
msgstr "มาสก์เครือข่าย"

#: ui/main.qml:41
#, fuzzy, kde-format
#| msgid "Type"
msgctxt "@label"
msgid "Type:"
msgstr "ชนิด"

#: ui/main.qml:45
#, kde-format
msgctxt "@label"
msgid "Hardware Address:"
msgstr ""

#: ui/main.qml:50
#, fuzzy, kde-format
#| msgid "State"
msgctxt "@label"
msgid "State:"
msgstr "สถานภาพ"

#: ui/main.qml:58
#, kde-format
msgctxt "State of network card is connected"
msgid "Up"
msgstr "ขึ้น"

#: ui/main.qml:59
#, kde-format
msgctxt "State of network card is disconnected"
msgid "Down"
msgstr "ลง"

#: ui/main.qml:70
#, kde-format
msgctxt "@action:button"
msgid "Refresh"
msgstr ""

#, fuzzy
#~| msgid "Network Mask"
#~ msgid "Network Interfaces"
#~ msgstr "มาสก์เครือข่าย"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ภูวณัฏฐ์ สาครสกลพัฒน์"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "narachai@gmail.com"

#, fuzzy
#~| msgid "Network Mask"
#~ msgctxt "@title:window"
#~ msgid "Network Interfaces"
#~ msgstr "มาสก์เครือข่าย"

#, fuzzy
#~| msgid "(c) 2001 - 2002 Alexander Neundorf"
#~ msgctxt "@info"
#~ msgid "(c) 2001 - 2002 Alexander Neundorf"
#~ msgstr "สงวนลิขสิทธิ์ (c) 2544 - 2545 Alexander Neundorf"

#, fuzzy
#~| msgid "Alexander Neundorf"
#~ msgctxt "@info:credit"
#~ msgid "Alexander Neundorf"
#~ msgstr "Alexander Neundorf"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Broadcast"
#~ msgstr "กระจายสัญญาณ"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Point to Point"
#~ msgstr "จุดต่อจุด"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Multicast"
#~ msgstr "มัลติแคสต์"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Loopback"
#~ msgstr "วนกลับ"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Unknown"
#~ msgstr "ไม่ทราบ"

#~ msgctxt "Unknown network mask"
#~ msgid "Unknown"
#~ msgstr "ไม่ทราบ"

#~ msgctxt "Unknown HWaddr"
#~ msgid "Unknown"
#~ msgstr "ไม่ทราบ"

#, fuzzy
#~| msgid "Broadcast"
#~ msgctxt "@item:intable Netork type"
#~ msgid "Broadcast"
#~ msgstr "กระจายสัญญาณ"

#~ msgid "HWAddr"
#~ msgstr "หมายเลขฮาร์ดแวร์"

#~ msgid "&Update"
#~ msgstr "&ปรับปรุง"

#~ msgid "kcminfo"
#~ msgstr "kcminfo"

#~ msgid "System Information Control Module"
#~ msgstr "โมดูลควบคุมรายละเอียดของระบบ"

# translation of kcmnic.po to Slovak
# Richard Fric <Richard.Fric@kdemail.net>, 2010.
# Michal Sulek <misurel@gmail.com>, 2010, 2011.
# Matej Mrenica <matejm98mthw@gmail.com>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kcmnic\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-05 02:14+0000\n"
"PO-Revision-Date: 2021-01-07 14:30+0100\n"
"Last-Translator: Matej Mrenica <matejm98mthw@gmail.com>\n"
"Language-Team: Slovak <kde-i18n-doc@kde.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: networkmodel.cpp:161
#, kde-format
msgctxt "@item:intable Network type"
msgid "Point to Point"
msgstr "Point to Point"

#: networkmodel.cpp:168
#, kde-format
msgctxt "@item:intable Network type"
msgid "Broadcast"
msgstr "Broadcast"

#: networkmodel.cpp:175
#, kde-format
msgctxt "@item:intable Network type"
msgid "Multicast"
msgstr "Multicast"

#: networkmodel.cpp:182
#, kde-format
msgctxt "@item:intable Network type"
msgid "Loopback"
msgstr "Loopback"

#: ui/main.qml:29
#, kde-format
msgctxt "@label"
msgid "Name:"
msgstr "Názov:"

#: ui/main.qml:33
#, kde-format
msgctxt "@label"
msgid "Address:"
msgstr "Adresa:"

#: ui/main.qml:37
#, kde-format
msgctxt "@label"
msgid "Network Mask:"
msgstr "Sieťová maska:"

#: ui/main.qml:41
#, kde-format
msgctxt "@label"
msgid "Type:"
msgstr "Typ:"

#: ui/main.qml:45
#, kde-format
msgctxt "@label"
msgid "Hardware Address:"
msgstr "Hardvérová adresa:"

#: ui/main.qml:50
#, kde-format
msgctxt "@label"
msgid "State:"
msgstr "Stav:"

#: ui/main.qml:58
#, kde-format
msgctxt "State of network card is connected"
msgid "Up"
msgstr "Beží"

#: ui/main.qml:59
#, kde-format
msgctxt "State of network card is disconnected"
msgid "Down"
msgstr "Nebeží"

#: ui/main.qml:70
#, kde-format
msgctxt "@action:button"
msgid "Refresh"
msgstr "Obnoviť"

#~ msgid "Network Interfaces"
#~ msgstr "Sieťové rozhrania"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Michal Šulek"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "misurel@gmail.com"

#~ msgid "kcm_nic"
#~ msgstr "kcm_nic"

#~ msgctxt "@title:window"
#~ msgid "Network Interfaces"
#~ msgstr "Sieťové rozhrania"

#~ msgctxt "@info"
#~ msgid "(c) 2001 - 2002 Alexander Neundorf"
#~ msgstr "(c) 2001 - 2002 Alexander Neundorf"

#~ msgctxt "@info:credit"
#~ msgid "Alexander Neundorf"
#~ msgstr "Alexander Neundorf"

#~ msgctxt "@info:credit"
#~ msgid "creator"
#~ msgstr "creator"

#~ msgctxt "@info:credit"
#~ msgid "Carl Schwan"
#~ msgstr "Carl Schwan"

#~ msgctxt "@info:credit"
#~ msgid "developer"
#~ msgstr "developer"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Broadcast"
#~ msgstr "Broadcast"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Point to Point"
#~ msgstr "Point to Point"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Multicast"
#~ msgstr "Multicast"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Loopback"
#~ msgstr "Loopback"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Unknown"
#~ msgstr "Neznámy"

#~ msgctxt "Unknown network mask"
#~ msgid "Unknown"
#~ msgstr "Neznáma"

#~ msgctxt "Unknown HWaddr"
#~ msgid "Unknown"
#~ msgstr "Neznáma"

#, fuzzy
#~| msgid "Broadcast"
#~ msgctxt "@item:intable Netork type"
#~ msgid "Broadcast"
#~ msgstr "Broadcast"

#~ msgid "HWAddr"
#~ msgstr "HW adresa"

#~ msgid "&Update"
#~ msgstr "Akt&ualizovať"

#~ msgid "kcminfo"
#~ msgstr "kcminfo"

#~ msgid "System Information Control Module"
#~ msgstr "Modul ovládacieho centra pre systémové informácie"
